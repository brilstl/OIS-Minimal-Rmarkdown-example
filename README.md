# OIS-Minimal-Rmarkdown-example
<br>

A minimal example of how to create _and_ host a Rmarkdown file on Github. The document is shown via this link:

- https://brilstl.gitlab.io/OIS-Minimal-Rmarkdown-example/minimal-Rmarkdown-example.html

---

# step 1: create a html document

The first step is to create a document which is able to be published on the internet. In this example a html-file is created through `R`. Before pushing to github, make sure that all the code is working properly.

---

# step 2: add .gitlab-ci.yml

Create a `.gitlab-ci.yml` file in which a docker container specifies how to deploy the `.Rmd` script.

**TO DO** c

- understand how to work with packages from `devtools::install_github`.